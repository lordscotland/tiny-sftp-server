#include <stddef.h>
#include <stdint.h>

struct scanner {
  const char* ptr;
  size_t remain;
};

int scanner_get_char(struct scanner* scanner,char* vptr);
int scanner_get_uint32(struct scanner* scanner,uint32_t* vptr);
int scanner_get_uint64(struct scanner* scanner,uint64_t* vptr);
const void* scanner_get_string(struct scanner* scanner,uint32_t* lptr);
