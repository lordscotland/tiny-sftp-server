#include <stdint.h>

struct packet_node;
struct buffer {
  struct packet_node* current;
  struct packet_node* head;
  struct packet_node* tail;
  size_t nbytes;
  uint32_t offset;
};

void buffer_init(struct buffer* buffer);
void* buffer_reserve(struct buffer* buffer,uint32_t datalen);
void* buffer_resize(struct buffer* buffer,uint32_t datalen);
int buffer_commit(struct buffer* buffer,uint8_t type);
ssize_t buffer_write(struct buffer* buffer,int fd);
void buffer_destroy(struct buffer* buffer);
