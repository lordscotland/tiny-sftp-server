#include <dirent.h>
#include <stdlib.h>
#include <unistd.h>
#include "handles.h"

struct handle_item {
  uintptr_t value;
  enum handle_type {
    HANDLE_TYPE_NULL,
    HANDLE_TYPE_DIR,
    HANDLE_TYPE_FD
  } type;
};

void handles_init(struct handles* handles) {
  handles->array=NULL;
  handles->count=handles->inull=0;
}
static size_t handles_add(struct handles* handles,enum handle_type type,uintptr_t value) {
  size_t index=handles->inull;
  struct handle_item* item;
  if(index){
    item=&handles->array[index-1];
    handles->inull=item->value;
  }
  else {
    index=handles->count+1;
    if(index==0 || index>SIZE_MAX/sizeof(*item)){return 0;}
    struct handle_item* array=realloc(handles->array,index*sizeof(*item));
    if(!array){return 0;}
    handles->array=array;
    handles->count=index;
    item=&array[index-1];
  }
  item->type=type;
  item->value=value;
  return index;
}
size_t handles_add_dir(struct handles* handles,DIR* dir) {
  return handles_add(handles,HANDLE_TYPE_DIR,(uintptr_t)dir);
}
size_t handles_add_fd(struct handles* handles,int fd) {
  return handles_add(handles,HANDLE_TYPE_FD,fd);
}
static uintptr_t handles_get(struct handles* handles,enum handle_type type,size_t index){
  if(index>0 && index<=handles->count){
    struct handle_item* item=&handles->array[index-1];
    if(item->type==type){return item->value;}
  }
  return (type==HANDLE_TYPE_FD)?-1:0;
}
DIR* handles_get_dir(struct handles* handles,size_t index) {
  return (DIR*)handles_get(handles,HANDLE_TYPE_DIR,index);
}
int handles_get_fd(struct handles* handles,size_t index) {
  return handles_get(handles,HANDLE_TYPE_FD,index);
}
static int handle_item_close(struct handle_item* item) {
  switch(item->type){
    case HANDLE_TYPE_DIR:return closedir((DIR*)item->value)==0;
    case HANDLE_TYPE_FD:return close(item->value)==0;
    default:return -1;
  }
}
int handles_remove(struct handles* handles,size_t index) {
  if(index>0 && index<=handles->count){
    struct handle_item* item=&handles->array[index-1];
    int ok=handle_item_close(item);
    if(ok!=-1){
      item->type=HANDLE_TYPE_NULL;
      item->value=handles->inull;
      handles->inull=index;
      return ok;
    }
  }
  return -1;
}
void handles_destroy(struct handles* handles) {
  for (size_t i=0,imax=handles->count;i<imax;i++){handle_item_close(&handles->array[i]);}
  free(handles->array);
}
