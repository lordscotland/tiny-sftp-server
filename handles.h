#include <dirent.h>
#include <stdint.h>

struct handle_item;
struct handles {
  struct handle_item* array;
  size_t count,inull;
};

void handles_init(struct handles* handles);
size_t handles_add_dir(struct handles* handles,DIR* dir);
size_t handles_add_fd(struct handles* handles,int fd);
DIR* handles_get_dir(struct handles* handles,size_t index);
int handles_get_fd(struct handles* handles,size_t index);
int handles_remove(struct handles* handles,size_t index);
void handles_destroy(struct handles* handles);
