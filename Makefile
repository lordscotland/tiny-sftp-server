CFLAGS+=-Wall -Wextra

obj/sftp-server: $(patsubst %.c,obj/%.o,$(wildcard *.c))
	$(CC) $(CFLAGS) -s -o $@ $^
obj/%.o: %.c
	@mkdir -p obj
	$(CC) $(CFLAGS) -c -o $@ $<
