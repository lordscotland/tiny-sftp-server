#include <endian.h>
#include "scanner.h"

static const void* scanner_get(struct scanner* scanner,size_t size) {
  if(scanner->remain<size){return NULL;}
  const void* ptr=scanner->ptr;
  scanner->ptr+=size;
  scanner->remain-=size;
  return ptr;
}
int scanner_get_char(struct scanner* scanner,char* vptr) {
  const char* ptr=scanner_get(scanner,sizeof(*ptr));
  if(!ptr){return 0;}
  *vptr=*ptr;
  return 1;
}
int scanner_get_uint32(struct scanner* scanner,uint32_t* vptr) {
  const uint32_t* ptr=scanner_get(scanner,sizeof(*ptr));
  if(!ptr){return 0;}
  *vptr=be32toh(*ptr);
  return 1;
}
int scanner_get_uint64(struct scanner* scanner,uint64_t* vptr) {
  const uint64_t* ptr=scanner_get(scanner,sizeof(*ptr));
  if(!ptr){return 0;}
  *vptr=be64toh(*ptr);
  return 1;
}
const void* scanner_get_string(struct scanner* scanner,uint32_t* lptr) {
  return scanner_get_uint32(scanner,lptr)?scanner_get(scanner,*lptr):NULL;
}
