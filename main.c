#include <endian.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include "buffer.h"
#include "handles.h"
#include "scanner.h"

#define SSH_FXP_INIT                1
#define SSH_FXP_VERSION             2
#define SSH_FXP_OPEN                3
#define SSH_FXP_CLOSE               4
#define SSH_FXP_READ                5
#define SSH_FXP_WRITE               6
#define SSH_FXP_LSTAT               7
#define SSH_FXP_FSTAT               8
#define SSH_FXP_SETSTAT             9
#define SSH_FXP_FSETSTAT           10
#define SSH_FXP_OPENDIR            11
#define SSH_FXP_READDIR            12
#define SSH_FXP_REMOVE             13
#define SSH_FXP_MKDIR              14
#define SSH_FXP_RMDIR              15
#define SSH_FXP_REALPATH           16
#define SSH_FXP_STAT               17
#define SSH_FXP_RENAME             18
#define SSH_FXP_READLINK           19
#define SSH_FXP_SYMLINK            20
#define SSH_FXP_STATUS            101
#define SSH_FXP_HANDLE            102
#define SSH_FXP_DATA              103
#define SSH_FXP_NAME              104
#define SSH_FXP_ATTRS             105
#define SSH_FXP_EXTENDED          200
#define SSH_FXP_EXTENDED_REPLY    201

#define SSH_FILEXFER_ATTR_SIZE          0x00000001
#define SSH_FILEXFER_ATTR_UIDGID        0x00000002
#define SSH_FILEXFER_ATTR_PERMISSIONS   0x00000004
#define SSH_FILEXFER_ATTR_ACMODTIME     0x00000008
#define SSH_FILEXFER_ATTR_EXTENDED      0x80000000

#define SSH_FXF_READ            0x00000001
#define SSH_FXF_WRITE           0x00000002
#define SSH_FXF_APPEND          0x00000004
#define SSH_FXF_CREAT           0x00000008
#define SSH_FXF_TRUNC           0x00000010
#define SSH_FXF_EXCL            0x00000020

#define SSH_FX_OK                            0
#define SSH_FX_EOF                           1
#define SSH_FX_NO_SUCH_FILE                  2
#define SSH_FX_PERMISSION_DENIED             3
#define SSH_FX_FAILURE                       4
#define SSH_FX_BAD_MESSAGE                   5
#define SSH_FX_NO_CONNECTION                 6
#define SSH_FX_CONNECTION_LOST               7
#define SSH_FX_OP_UNSUPPORTED                8

#define ATTRS_MAXSIZE sizeof(uint64_t)+6*sizeof(uint32_t)

struct attrs {
  uint64_t size;
  uint32_t uid;
  uint32_t gid;
  uint32_t perm;
  uint32_t atime;
  uint32_t mtime;
  uint32_t flags;
};
struct context {
  struct buffer buffer;
  struct handles handles;
  int init;
};
static void print_unsigned(char* buf,size_t len,unsigned value,int left) {
  for (size_t i=len;i--;){
    unsigned next=value/10;
    buf[i]='0'+(value-next*10);
    if(!(value=next)){
      if(left){
        memmove(buf,buf+i,len-i);
        memset(buf+len-i,' ',i);
      }
      else {memset(buf,' ',i);}
      return;
    }
  }
  memset(buf,'*',len);
}
static void* write_stat(void* optr,struct stat* st) {
  uint8_t* ptr=optr;
  *(uint32_t*)ptr=htobe32(SSH_FILEXFER_ATTR_SIZE|SSH_FILEXFER_ATTR_UIDGID
   |SSH_FILEXFER_ATTR_PERMISSIONS|SSH_FILEXFER_ATTR_ACMODTIME);ptr+=sizeof(uint32_t);
  *(uint64_t*)ptr=htobe64(st->st_size);ptr+=sizeof(uint64_t);
  *(uint32_t*)ptr=htobe32(st->st_uid);ptr+=sizeof(uint32_t);
  *(uint32_t*)ptr=htobe32(st->st_gid);ptr+=sizeof(uint32_t);
  *(uint32_t*)ptr=htobe32(st->st_mode);ptr+=sizeof(uint32_t);
  *(uint32_t*)ptr=htobe32(st->st_atime);ptr+=sizeof(uint32_t);
  *(uint32_t*)ptr=htobe32(st->st_mtime);ptr+=sizeof(uint32_t);
  return ptr;
}
static int scanner_get_handle(struct scanner* scanner,size_t* iptr) {
  uint32_t indexlen;
  const size_t* indexptr=scanner_get_string(scanner,&indexlen);
  if(!indexptr || indexlen!=sizeof(*indexptr)){return 0;}
  *iptr=*indexptr;
  return 1;
}
static int scanner_get_path(struct scanner* scanner,char* path) {
  uint32_t pslen;
  const char* psptr=scanner_get_string(scanner,&pslen);
  if(!psptr || pslen>PATH_MAX-1){return 0;}
  memcpy(path,psptr,pslen);
  path[pslen]=0;
  return 1;
}
static int scanner_get_attrs(struct scanner* scanner,struct attrs* attrs) {
  if(!scanner_get_uint32(scanner,&attrs->flags)){return 0;}
  if(attrs->flags&SSH_FILEXFER_ATTR_SIZE
   && !scanner_get_uint64(scanner,&attrs->size)){return 0;}
  if(attrs->flags&SSH_FILEXFER_ATTR_UIDGID
   && (!scanner_get_uint32(scanner,&attrs->uid)
   || !scanner_get_uint32(scanner,&attrs->gid))){return 0;}
  if(attrs->flags&SSH_FILEXFER_ATTR_PERMISSIONS
   && !scanner_get_uint32(scanner,&attrs->perm)){return 0;}
  if(attrs->flags&SSH_FILEXFER_ATTR_ACMODTIME
   && (!scanner_get_uint32(scanner,&attrs->atime)
   || !scanner_get_uint32(scanner,&attrs->mtime))){return 0;}
  if(attrs->flags&SSH_FILEXFER_ATTR_EXTENDED){
    uint32_t count;
    if(!scanner_get_uint32(scanner,&count)){return 0;}
    for (count*=2;count>0;count--){
      uint32_t len;
      if(!scanner_get_string(scanner,&len)){return 0;}
    }
  }
  return 1;
}
static int buffer_commit_status(struct buffer* buffer,uint32_t id,uint32_t status,const char* errmsg,const char* lang) {
  const uint32_t errmsglen=errmsg?strlen(errmsg):0,langlen=lang?strlen(lang):0;
  uint8_t* ptr=buffer_reserve(buffer,sizeof(id)+sizeof(status)
   +sizeof(errmsglen)+errmsglen+sizeof(langlen)+langlen);
  if(!ptr){return 0;} 
  *(uint32_t*)ptr=htobe32(id);ptr+=sizeof(id);
  *(uint32_t*)ptr=htobe32(status);ptr+=sizeof(status);
  *(uint32_t*)ptr=htobe32(errmsglen);ptr+=sizeof(errmsglen);
  memcpy(ptr,errmsg,errmsglen);ptr+=errmsglen;
  *(uint32_t*)ptr=htobe32(langlen);ptr+=sizeof(langlen);
  memcpy(ptr,lang,langlen);
  return buffer_commit(buffer,SSH_FXP_STATUS);
}
static int buffer_commit_error(struct buffer* buffer,uint32_t id,int errnum) {
  uint32_t status;
  switch(errnum){
    case ENOENT:case ENOTDIR:case EBADF:case ELOOP:status=SSH_FX_NO_SUCH_FILE;break;
    case EPERM:case EACCES:case EFAULT:status=SSH_FX_PERMISSION_DENIED;break;
    case ENAMETOOLONG:case EINVAL:status=SSH_FX_BAD_MESSAGE;break;
    case ENOSYS:status=SSH_FX_OP_UNSUPPORTED;break;
    default:status=SSH_FX_FAILURE;break;
  }
  return buffer_commit_status(buffer,id,status,strerror(errnum),NULL);
}
static int buffer_commit_handle(struct buffer* buffer,uint32_t id,size_t index) {
  uint8_t* ptr=buffer_reserve(buffer,sizeof(id)+sizeof(uint32_t)+sizeof(index));
  if(!ptr){return 0;}
  *(uint32_t*)ptr=htobe32(id);ptr+=sizeof(id);
  *(uint32_t*)ptr=htobe32(sizeof(index));ptr+=sizeof(uint32_t);
  *(size_t*)ptr=index;
  return buffer_commit(buffer,SSH_FXP_HANDLE);
}
static int buffer_commit_name(struct buffer* buffer,uint32_t id,const char* name) {
  const uint32_t namelen=name?strlen(name):0;
  uint8_t* ptr=buffer_reserve(buffer,sizeof(id)+sizeof(uint32_t)
   +sizeof(namelen)+namelen+2*sizeof(uint32_t));
  if(!ptr){return 0;} 
  *(uint32_t*)ptr=htobe32(id);ptr+=sizeof(id);
  *(uint32_t*)ptr=htobe32(1);ptr+=sizeof(uint32_t);
  *(uint32_t*)ptr=htobe32(namelen);ptr+=sizeof(namelen);
  memcpy(ptr,name,namelen);ptr+=namelen;
  uint32_t* iptr=(void*)ptr;
  iptr[0]=iptr[1]=0;
  return buffer_commit(buffer,SSH_FXP_NAME);
}
static int buffer_commit_attrs(struct buffer* buffer,uint32_t id,struct stat* st) {
  uint8_t* ptr=buffer_reserve(buffer,sizeof(id)+ATTRS_MAXSIZE);
  if(!ptr){return 0;}
  *(uint32_t*)ptr=htobe32(id);ptr+=sizeof(id);
  write_stat(ptr,st);
  return buffer_commit(buffer,SSH_FXP_ATTRS);
}
static int process_input(struct scanner* scanner,struct context* context) {
  char type;
  uint32_t id;
  if(!scanner_get_char(scanner,&type) || !scanner_get_uint32(scanner,&id)){return 0;}
  if(!context->init){
    const uint32_t version=3;
    if(type!=SSH_FXP_INIT || id<version){return 0;}
    uint32_t* ptr=buffer_reserve(&context->buffer,sizeof(version));
    if(!ptr){return 0;}
    *ptr=htobe32(version);
    context->init=1;
    return buffer_commit(&context->buffer,SSH_FXP_VERSION);
  }
  else if(type==SSH_FXP_OPEN){
    char path[PATH_MAX];
    uint32_t pflags;
    struct attrs V_attrs;
    if(scanner_get_path(scanner,path) && scanner_get_uint32(scanner,&pflags)
     && scanner_get_attrs(scanner,&V_attrs)){
      int oflags=0;
      switch(pflags&(SSH_FXF_READ|SSH_FXF_WRITE)){
        case SSH_FXF_READ:oflags|=O_RDONLY;break;
        case SSH_FXF_WRITE:oflags|=O_WRONLY;break;
        case SSH_FXF_READ|SSH_FXF_WRITE:oflags|=O_RDWR;break;
      }
      if(pflags&SSH_FXF_APPEND){oflags|=O_APPEND;}
      if(pflags&SSH_FXF_CREAT){oflags|=O_CREAT;}
      if(pflags&SSH_FXF_TRUNC){oflags|=O_TRUNC;}
      if(pflags&SSH_FXF_EXCL){oflags|=O_EXCL;}
      do {
        int fd=open(path,oflags,(V_attrs.flags&SSH_FILEXFER_ATTR_PERMISSIONS)?
         V_attrs.perm:0666);
        if(fd<0){break;}
        size_t index=handles_add_fd(&context->handles,fd);
        if(index){return buffer_commit_handle(&context->buffer,id,index);}
        if(close(fd)<0){break;}
        errno=ENOMEM;
      } while(0);
      return buffer_commit_error(&context->buffer,id,errno);
    }
  }
  else if(type==SSH_FXP_CLOSE){
    size_t index;
    if(scanner_get_handle(scanner,&index)){
      int ok=handles_remove(&context->handles,index);
      return (ok>0)?buffer_commit_status(&context->buffer,id,SSH_FX_OK,NULL,NULL):
       buffer_commit_error(&context->buffer,id,ok?EBADF:errno);
    }
  }
  else if(type==SSH_FXP_READ){
    size_t index;
    uint64_t offset;
    uint32_t len;
    if(scanner_get_handle(scanner,&index) && scanner_get_uint64(scanner,&offset)
     && scanner_get_uint32(scanner,&len)){
      do {
        int fd=handles_get_fd(&context->handles,index);
        if(fd<0){errno=EBADF;break;}
        if(lseek(fd,offset,SEEK_SET)<0){break;}
        const uint32_t prelen=sizeof(uint32_t)*2;
        uint8_t* ptr=buffer_reserve(&context->buffer,prelen+len);
        if(!ptr){break;}
        ssize_t rlen=read(fd,ptr+prelen,len);
        if(rlen<0){break;}
        if(rlen==0){return buffer_commit_status(&context->buffer,id,SSH_FX_EOF,NULL,NULL);}
        if(!(ptr=buffer_resize(&context->buffer,prelen+rlen))){break;}
        *(uint32_t*)ptr=htobe32(id);ptr+=sizeof(id);
        *(uint32_t*)ptr=htobe32(rlen);
        return buffer_commit(&context->buffer,SSH_FXP_DATA);
      } while(0);
      return buffer_commit_error(&context->buffer,id,errno);
    }
  }
  else if(type==SSH_FXP_WRITE){
    size_t index;
    uint64_t offset;
    uint32_t datalen;
    const void* dataptr;
    if(scanner_get_handle(scanner,&index) && scanner_get_uint64(scanner,&offset)
     && (dataptr=scanner_get_string(scanner,&datalen))){
      do {
        int fd=handles_get_fd(&context->handles,index);
        if(fd<0){errno=EBADF;break;}
        if(lseek(fd,offset,SEEK_SET)<0){break;}
        ssize_t wlen=write(fd,dataptr,datalen);
        if(wlen<0){break;}
        return buffer_commit_status(&context->buffer,id,
         ((uint32_t)wlen==datalen)?SSH_FX_OK:SSH_FX_EOF,NULL,NULL);
      } while(0);
      return buffer_commit_error(&context->buffer,id,errno);
    }
  }
  else if(type==SSH_FXP_LSTAT || type==SSH_FXP_STAT){
    char path[PATH_MAX];
    if(scanner_get_path(scanner,path)){
      struct stat V_st;
      return ((type==SSH_FXP_LSTAT?lstat(path,&V_st):stat(path,&V_st))<0)?
       buffer_commit_error(&context->buffer,id,errno):
       buffer_commit_attrs(&context->buffer,id,&V_st);
    }
  }
  else if(type==SSH_FXP_FSTAT){
    size_t index;
    if(scanner_get_handle(scanner,&index)){
      do {
        int fd=handles_get_fd(&context->handles,index);
        if(fd<0){errno=EBADF;break;}
        struct stat V_st;
        if(fstat(fd,&V_st)<0){break;}
        return buffer_commit_attrs(&context->buffer,id,&V_st);
      } while(0);
      return buffer_commit_error(&context->buffer,id,errno);
    }
  }
  else if(type==SSH_FXP_SETSTAT){
    char path[PATH_MAX];
    struct attrs V_attrs;
    if(scanner_get_path(scanner,path) && scanner_get_attrs(scanner,&V_attrs)){
      return ((V_attrs.flags&SSH_FILEXFER_ATTR_SIZE && truncate(path,V_attrs.size)<0)
       || (V_attrs.flags&SSH_FILEXFER_ATTR_UIDGID && chown(path,V_attrs.uid,V_attrs.gid)<0)
       || (V_attrs.flags&SSH_FILEXFER_ATTR_PERMISSIONS && chmod(path,V_attrs.perm)<0)
       || (V_attrs.flags&SSH_FILEXFER_ATTR_ACMODTIME && utimes(path,
       (struct timeval[]){{.tv_sec=V_attrs.atime},{.tv_sec=V_attrs.mtime}})<0))?
       buffer_commit_error(&context->buffer,id,errno):
       buffer_commit_status(&context->buffer,id,SSH_FX_OK,NULL,NULL);
    }
  }
  else if(type==SSH_FXP_FSETSTAT){
    size_t index;
    struct attrs V_attrs;
    if(scanner_get_handle(scanner,&index) && scanner_get_attrs(scanner,&V_attrs)){
      do {
        int fd=handles_get_fd(&context->handles,index);
        if(fd<0){errno=EBADF;break;}
        if((V_attrs.flags&SSH_FILEXFER_ATTR_SIZE && ftruncate(fd,V_attrs.size)<0)
         || (V_attrs.flags&SSH_FILEXFER_ATTR_UIDGID && fchown(fd,V_attrs.uid,V_attrs.gid)<0)
         || (V_attrs.flags&SSH_FILEXFER_ATTR_PERMISSIONS && fchmod(fd,V_attrs.perm)<0)
         || (V_attrs.flags&SSH_FILEXFER_ATTR_ACMODTIME && futimes(fd,
         (struct timeval[]){{.tv_sec=V_attrs.atime},{.tv_sec=V_attrs.mtime}})<0)){break;}
        return buffer_commit_status(&context->buffer,id,SSH_FX_OK,NULL,NULL);
      } while(0);
      return buffer_commit_error(&context->buffer,id,errno);
    }
  }
  else if(type==SSH_FXP_OPENDIR){
    char path[PATH_MAX];
    if(scanner_get_path(scanner,path)){
      do {
        DIR* dir=opendir(path);
        if(!dir){break;}
        size_t index=handles_add_dir(&context->handles,dir);
        if(index){return buffer_commit_handle(&context->buffer,id,index);}
        if(closedir(dir)<0){break;}
        errno=ENOMEM;
      } while(0);
      return buffer_commit_error(&context->buffer,id,errno);
    }
  }
  else if(type==SSH_FXP_READDIR){
    size_t index;
    if(scanner_get_handle(scanner,&index)){
      do {
        DIR* dir=handles_get_dir(&context->handles,index);
        if(!dir){errno=EBADF;break;}
        uint32_t count=0;
        size_t size=sizeof(id)+sizeof(count);
        struct info {
          char mode[10],z1;
          char nlink[4],z2;
          char user[8],z3;
          char group[8],z4;
          char size[8],z5;
          char mtime[12],z6;
        };
        struct name_node {
          struct name_node* next;
          char name[];
        };
        struct name_node* head=NULL;
        struct name_node* tail=NULL;
        struct dirent* entry;
        while((entry=readdir(dir))){
          uint32_t namelen=strlen(entry->d_name);
          struct name_node* node=malloc(sizeof(*node)+namelen+1);
          memcpy(node->name,entry->d_name,namelen+1);
          tail=*(head?&tail->next:&head)=node;
          count++;
          size+=2*(sizeof(namelen)+namelen)+sizeof(struct info)+ATTRS_MAXSIZE;
          if(size>0xFFFF){break;}
        }
        if(!head){return buffer_commit_status(&context->buffer,id,SSH_FX_EOF,NULL,NULL);}
        uint8_t* ptr=buffer_reserve(&context->buffer,size);
        if(!ptr){
          int errnum=errno;
          do {
            struct name_node* next=(head==tail)?NULL:head->next;
            free(head);
            head=next;
          } while(head);
          errno=errnum;
          break;
        }
        uint8_t* optr=ptr;
        int fd=dirfd(dir);
        *(uint32_t*)ptr=htobe32(id);ptr+=sizeof(id);
        *(uint32_t*)ptr=htobe32(count);ptr+=sizeof(count);
        do {
          const uint32_t namelen=strlen(head->name);
          *(uint32_t*)ptr=htobe32(namelen);ptr+=sizeof(namelen);
          memcpy(ptr,head->name,namelen);ptr+=namelen;
          const uint32_t longnamelen=sizeof(struct info)+namelen;
          *(uint32_t*)ptr=htobe32(longnamelen);ptr+=sizeof(longnamelen);
          struct info* info=(void*)ptr;ptr+=sizeof(*info);
          memcpy(ptr,head->name,namelen);ptr+=namelen;
          struct stat V_st;
          if(fstatat(fd,head->name,&V_st,AT_SYMLINK_NOFOLLOW)==0){
            char ftype;
            switch(V_st.st_mode&S_IFMT){
              case S_IFSOCK:ftype='s';break;
              case S_IFLNK:ftype='l';break;
              case S_IFREG:ftype='-';break;
              case S_IFBLK:ftype='b';break;
              case S_IFDIR:ftype='d';break;
              case S_IFCHR:ftype='c';break;
              case S_IFIFO:ftype='p';break;
              default:ftype='?';break;
            }
            info->mode[0]=ftype;
            info->mode[1]=(V_st.st_mode&S_IRUSR)?'r':'-';
            info->mode[2]=(V_st.st_mode&S_IWUSR)?'w':'-';
            info->mode[3]=(V_st.st_mode&S_ISUID)?
             (V_st.st_mode&S_IXUSR)?'s':'S':(V_st.st_mode&S_IXUSR)?'x':'-';
            info->mode[4]=(V_st.st_mode&S_IRGRP)?'r':'-';
            info->mode[5]=(V_st.st_mode&S_IWGRP)?'w':'-';
            info->mode[6]=(V_st.st_mode&S_ISGID)?
             (V_st.st_mode&S_IXGRP)?'s':'S':(V_st.st_mode&S_IXGRP)?'x':'-';
            info->mode[7]=(V_st.st_mode&S_IROTH)?'r':'-';
            info->mode[8]=(V_st.st_mode&S_IWOTH)?'w':'-';
            info->mode[9]=(V_st.st_mode&S_ISVTX)?
             (V_st.st_mode&S_IXOTH)?'t':'T':(V_st.st_mode&S_IXOTH)?'x':'-';
            print_unsigned(info->nlink,sizeof(info->nlink),V_st.st_nlink,0);
            print_unsigned(info->user,sizeof(info->user),V_st.st_uid,1);
            print_unsigned(info->group,sizeof(info->group),V_st.st_gid,1);
            print_unsigned(info->size,sizeof(info->size),V_st.st_size,0);
            time_t now;
            time(&now);
            size_t flen=strftime(info->mtime,sizeof(info->mtime)+1,
             (V_st.st_mtime>now-60*60*24*365/2 && V_st.st_mtime<=now)?
             "%b %e %H:%M":"%b %e  %Y",localtime(&V_st.st_mtime));
            if(flen==0){memset(info->mtime,'*',sizeof(info->mtime));}
            else if(flen<sizeof(info->mtime)){
              memset(info->mtime+flen,' ',sizeof(info->mtime)-flen);
            }
            info->z1=info->z2=info->z3=info->z4=info->z5=info->z6=' ';
            ptr=write_stat(ptr,&V_st);
          }
          else {
            memset(info,' ',sizeof(*info));
            *(uint32_t*)ptr=0;ptr+=sizeof(uint32_t);
          }
          struct name_node* next=(head==tail)?NULL:head->next;
          free(head);
          head=next;
        } while(head);
        if(!buffer_resize(&context->buffer,ptr-optr)){break;}
        return buffer_commit(&context->buffer,SSH_FXP_NAME);
      } while(0);
      return buffer_commit_error(&context->buffer,id,errno);
    }
  }
  else if(type==SSH_FXP_REMOVE || type==SSH_FXP_RMDIR){
    char path[PATH_MAX];
    if(scanner_get_path(scanner,path)){
      return ((type==SSH_FXP_REMOVE?unlink(path):rmdir(path))<0)?
       buffer_commit_error(&context->buffer,id,errno):
       buffer_commit_status(&context->buffer,id,SSH_FX_OK,NULL,NULL);
    }
  }
  else if(type==SSH_FXP_MKDIR){
    char path[PATH_MAX];
    struct attrs V_attrs;
    if(scanner_get_path(scanner,path) && scanner_get_attrs(scanner,&V_attrs)){
      return (mkdir(path,(V_attrs.flags&SSH_FILEXFER_ATTR_PERMISSIONS)?
       V_attrs.perm:0777)<0)?
       buffer_commit_error(&context->buffer,id,errno):
       buffer_commit_status(&context->buffer,id,SSH_FX_OK,NULL,NULL);
    }
  }
  else if(type==SSH_FXP_REALPATH){
    uint32_t pslen;
    const char* psptr=scanner_get_string(scanner,&pslen);
    if(psptr){
      char path[PATH_MAX];
      char* pathptr=path;
      if(pslen && psptr[0]=='/'){*pathptr++='/';}
      else if(!getcwd(path,sizeof(path))){
        return buffer_commit_error(&context->buffer,id,errno);
      }
      else {pathptr=strchr(pathptr,0);}
      char* pathend=path+sizeof(path)-1;
      const char* psend=psptr+pslen;
      while(psptr<psend){
        if(*psptr=='/'){psptr++;continue;}
        size_t len=psend-psptr;
        char* next=memchr(psptr,'/',len);
        if(next){len=next-psptr;}
        do {
          if(psptr[0]=='.'){
            if(len==1){break;}
            if(len==2 && psptr[1]=='.'){
              *pathptr=0;
              if(access(path,F_OK)<0){
                return buffer_commit_error(&context->buffer,id,errno);
              }
              while(pathptr>path+1 && *pathptr!='/'){pathptr--;}
              break;
            }
          }
          int slash=(pathptr>path+1)?1:0;
          if(pathptr+slash+len>=pathend){
            return buffer_commit_error(&context->buffer,id,ENAMETOOLONG);
          }
          if(slash){*pathptr++='/';}
          memcpy(pathptr,psptr,len);
          pathptr+=len;
        } while(0);
        if(!next){break;}
        psptr=next+1;
      }
      *pathptr=0;
      return buffer_commit_name(&context->buffer,id,path);
    }
  }
  else if(type==SSH_FXP_RENAME){
    char path0[PATH_MAX],path1[PATH_MAX];
    if(scanner_get_path(scanner,path0) && scanner_get_path(scanner,path1)){
      int errnum=0;
      if(link(path0,path1)<0){
        if(errno==EEXIST || access(path1,F_OK)==0){errnum=EEXIST;}
        else if(rename(path0,path1)<0){errnum=errno;}
      }
      else if(unlink(path0)<0){
        errnum=errno;
        unlink(path1);
      }
      return errnum?buffer_commit_error(&context->buffer,id,errnum):
       buffer_commit_status(&context->buffer,id,SSH_FX_OK,NULL,NULL);
    }
  }
  else if(type==SSH_FXP_READLINK){
    char path0[PATH_MAX],path1[PATH_MAX];
    if(scanner_get_path(scanner,path1)){
      ssize_t len=readlink(path1,path0,sizeof(path0)-1);
      if(len<0){return buffer_commit_error(&context->buffer,id,errno);}
      path0[len]=0;
      return buffer_commit_name(&context->buffer,id,path0);
    }
  }
  else if(type==SSH_FXP_SYMLINK){
    char path0[PATH_MAX],path1[PATH_MAX];
    if(scanner_get_path(scanner,path0) && scanner_get_path(scanner,path1)){
      return (symlink(path0,path1)<0)?
       buffer_commit_error(&context->buffer,id,errno):
       buffer_commit_status(&context->buffer,id,SSH_FX_OK,NULL,NULL);
    }
  }
  else {return buffer_commit_status(&context->buffer,id,SSH_FX_OP_UNSUPPORTED,NULL,NULL);}
  return buffer_commit_status(&context->buffer,id,SSH_FX_BAD_MESSAGE,NULL,NULL);
}

int main(int argc,char** argv) {
  const uid_t uid=getuid();
  struct passwd* pwd=getpwuid(uid);
  if(!pwd){perror("getpwuid");return -1;}
  const char* homedir=pwd->pw_dir;
  if(chdir(homedir)<0){perror("chdir");return -1;}
  if(!argc || strcmp(pwd->pw_shell,argv[argc-1])==0){chroot(homedir);}
  if(uid && setuid(uid)<0){perror("setuid");return -1;}
  struct context V_context;
  buffer_init(&V_context.buffer);
  handles_init(&V_context.handles);
  V_context.init=0;
  uint32_t length;
  char* bytes=NULL;
  char* cptr=(char*)&length;
  uint32_t cremain=sizeof(length);
  for (;;){
    fd_set rfds,wfds;
    FD_ZERO(&rfds);
    FD_ZERO(&wfds);
    int fdmax=-1;
    if(V_context.buffer.nbytes){
      FD_SET(STDOUT_FILENO,&wfds);
      if(fdmax<STDOUT_FILENO){fdmax=STDOUT_FILENO;}
    }
    if(V_context.buffer.nbytes<=0xFFFFFF){
      FD_SET(STDIN_FILENO,&rfds);
      if(fdmax<STDIN_FILENO){fdmax=STDIN_FILENO;}
    }
    if(select(fdmax+1,&rfds,&wfds,NULL,NULL)<0){
      perror("select");
      return -1;
    }
    if(FD_ISSET(STDOUT_FILENO,&wfds)
     && buffer_write(&V_context.buffer,STDOUT_FILENO)<0){
      perror("write");
      return -1;
    }
    if(FD_ISSET(STDIN_FILENO,&rfds)){
      char buf[4096];
      ssize_t rsize=read(STDIN_FILENO,buf,sizeof(buf));
      if(!rsize){break;}
      if(rsize<0){
        perror("read");
        return -1;
      }
      for (char* rptr=buf;rsize>0;){
        uint32_t len=cremain;
        if(len>(uint32_t)rsize){len=rsize;}
        memcpy(cptr,rptr,len);
        rptr+=len;
        rsize-=len;
        cptr+=len;
        if(cremain-=len){continue;}
        if(bytes){
          struct scanner V_scanner={.ptr=bytes,.remain=length};
          int success=process_input(&V_scanner,&V_context);
          free(bytes);
          bytes=NULL;
          if(!success){
            perror("process_input");
            return -1;
          }
          cptr=(char*)&length;
          cremain=sizeof(length);
        }
        else {
          if(!(length=be32toh(length))){break;}
          if(!(bytes=malloc(length))){
            perror("malloc");
            return -1;
          }
          cptr=bytes;
          cremain=length;
        }
      }
    }
  }
  buffer_destroy(&V_context.buffer);
  handles_destroy(&V_context.handles);
  free(bytes);
}
