#include <endian.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "buffer.h"

struct packet_node {
  struct packet_node* next;
  uint32_t length;
  uint8_t type;
  uint8_t data[];
};

void buffer_init(struct buffer* buffer) {
  buffer->current=buffer->head=NULL;
  buffer->nbytes=0;
  buffer->offset=0;
}
void* buffer_reserve(struct buffer* buffer,uint32_t datalen) {
  free(buffer->current);
  struct packet_node* node=buffer->current=malloc(sizeof(*node)+datalen);
  if(!node){return NULL;}
  node->length=datalen;
  return node->data;
}
void* buffer_resize(struct buffer* buffer,uint32_t datalen) {
  struct packet_node* node=buffer->current;
  if(!node){return NULL;}
  if(datalen!=node->length){
    if(!(node=realloc(node,sizeof(*node)+datalen))){return NULL;}
    buffer->current=node;
    node->length=datalen;
  }
  return node->data;
}
int buffer_commit(struct buffer* buffer,uint8_t type) {
  struct packet_node* node=buffer->current;
  if(!node){return 0;}
  size_t length=sizeof(node->type)+node->length;
  node->length=htobe32(length);
  node->type=type;
  buffer->nbytes+=sizeof(node->length)+length;
  buffer->tail=*(buffer->head?&buffer->tail->next:&buffer->head)=node;
  buffer->current=NULL;
  return 1;
}
ssize_t buffer_write(struct buffer* buffer,int fd) {
  struct packet_node* node=buffer->head;
  if(!node){return 0;}
  uint32_t wlen=sizeof(node->length)+be32toh(node->length)-buffer->offset;
  ssize_t wsize=write(fd,(char*)&node->length+buffer->offset,wlen);
  if(wsize>0){
    buffer->nbytes-=wsize;
    if((uint32_t)wsize<wlen){buffer->offset+=wsize;}
    else {
      buffer->offset=0;
      buffer->head=(node==buffer->tail)?NULL:node->next;
      free(node);
    }
  }
  return wsize;
}
void buffer_destroy(struct buffer* buffer) {
  free(buffer->current);
  struct packet_node* node=buffer->head;
  struct packet_node* tail=buffer->tail;
  while(node){
    struct packet_node* next=(node==tail)?NULL:node->next;
    free(node);
    node=next;
  }
}
